package giopaint;

public class Ponto3D {
    public int x, y, z;
    
    public Ponto3D(){
        x = y = z = 0;
    }
    
    public Ponto3D(int x, int y){
        this.x = x;
        this.y = y;
        this.z = 0;
    }
    
    public Ponto3D(int x, int y, int z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public Ponto3D(Ponto3D copia){
        this.x = copia.x;
        this.y = copia.y;
        this.z = copia.z;
    }
    
    public void mover(int x, int y, int z){
        this.x += x;
        this.y += y;
        this.z += z;
    }
    
    public void setLocation(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    public void setLocation(int x, int y, int z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public int getX() { return x; }
    public int getY() { return y; }
    public int getZ() { return z; }
    
    @Override
    public boolean equals(Object o){
        if (o instanceof Ponto3D){
            Ponto3D ponto = (Ponto3D) o;
            if (ponto.x == x && ponto.y == y && ponto.z == z) return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.x;
        hash = 37 * hash + this.y;
        hash = 37 * hash + this.z;
        return hash;
    }
    
    @Override
    public String toString(){
        return "(" + x + ", " + y + ", " + z + ")";
    }
}
