package giopaint;

import giopaint.formas.*;
import giopaint.funcoes.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;


public class Display extends JPanel implements MouseListener, KeyListener{
    
    private int x_clicado = 0, y_clicado = 0;
    private boolean recortado = false;
    private Poligono recorte;
    private Pixel [][] display;
    private static Display singleton; 
    private ArrayList<Forma> formas;
    private final int altura = 630, largura = 830, grossura = 10;
    private final int colunas = largura/grossura, linhas = altura/grossura;
    private static final long serialVersionUID = 1L;
    private final Dimension dimensao;
    private Modo modo = Modo.Idle;
    private Color cor_escolhida = Color.BLACK;
    private ArrayList<Ponto3D> pontos;
    private static int lados_poligono = 3;
    private static boolean grade = true;
    
    public enum Modo{
        Idle, Linha, Elipse, Circulo, PolilinhaAberta, Poligono, Polilinha,
        Retangulo, Preencher,Remover, Recorte, Escalar, Rotacionar, Transladar,
        Forma3D, Projecao,ProjecaoObliqua, Projecao2Pontos, Projecao3Pontos, ProjecaoIsometrica;
    }
    
    public Display(){
        pontos = new ArrayList<>();
        dimensao = new Dimension(largura, altura);
        formas = new ArrayList<>();
        carregarPixels();
        setFocusable(true);
        requestFocusInWindow();
        addMouseListener(this);
        addKeyListener(this);
    }
    
    @Override
    public Dimension getPreferredSize() {
        return this.dimensao;
    }

    public static Display instancia(){
        if (singleton == null){
            singleton = new Display();
        }
        return singleton;
    }
    
    public ArrayList<Forma> getFormas(){ return formas; }
    
    public int getAltura(){ return altura; }
    public int getLargura(){ return largura; }
    
    public void carregarPixels(){
        display = new Pixel[colunas+1][linhas+1];
        
        for (int x = 0 ; x <= colunas ; x++)
            for (int y = 0 ; y <= linhas ; y++)
                display[x][y] = new Pixel(x - colunas/2, -y + linhas/2, Color.WHITE);
    }
    
    public void escolherCor(){
        cor_escolhida = JColorChooser.showDialog(null, "Esolha uma Cor", Color.BLACK);
        if (cor_escolhida == null){
            cor_escolhida = Color.BLACK;
        }
    }

    public void apagarTudo(){
        formas.clear();
        carregarPixels();
        validate();
        repaint();
    }
    
    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        if (recortado){
            //carregarPixels();
            Funcao.recorte(recorte);
        }
        else{
            for (Forma x: formas)
                x.desenha();
        }
        
        for (Pixel[] px: display)
            for (Pixel py: px)
                pintarPixel(py.x, py.y, py.cor, g);
        
        if (grade) criarGrade(g);
    }
    
    public void pintarPixel(int x, int y, Color cor, Graphics g){
        g.setColor(cor);
        g.fillRect(( x + colunas/2 ) * grossura, ( y + linhas/2 )* grossura, grossura, grossura);
    }
    
    public Color lerPixel(int x, int y){
        try{
            return display[x + colunas/2][y + linhas/2].cor;
        }
        catch(ArrayIndexOutOfBoundsException e){
            return null;
        }   
    }
    
    // desenha a grade de pixels
    public void criarGrade(Graphics g) {
        int i;
        g.setColor(Color.LIGHT_GRAY);
 
        int linhas_altura = altura / linhas;
        for (i = 0; i < linhas; i++)
            g.drawLine(0, i * linhas_altura, largura, i * linhas_altura);

        int linhas_largura = largura / colunas;
        for (i = 0; i < colunas; i++)
            g.drawLine(i * linhas_largura, 0, i * linhas_largura, altura);
        
        // fazer os eixos
        g.setColor(Color.BLACK);
        g.drawLine(0, altura/2, largura, altura/2); // eixo x
        g.drawLine(largura/2, 0, largura/2, altura); // eixo y
        
    }

    public static String imprime(Ponto3D p){
        return "(" + p.x + ", " + p.y + ")";
    }
    
    public void adicionaPonto(int x, int y, Color cor){
        try{
            display[x + colunas/2][y + linhas/2].cor = cor;
        }
        catch(ArrayIndexOutOfBoundsException e){
        } 
    }
    
    public void adicionaPontos(Pixel vetor[]){
        for (Pixel p: vetor)
            adicionaPonto(p.x, p.y, p.cor);
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        x_clicado = me.getX()/grossura;
        y_clicado = me.getY()/grossura;

        tarefasModo();
        revalidate();
        repaint();
        
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
       
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        int id = ke.getKeyCode();
        switch (id) {
            case KeyEvent.VK_ENTER:
                if (modo == Modo.Polilinha){
                    formas.add(new Polilinha(pontos, cor_escolhida));
                }
                else if (modo == Modo.PolilinhaAberta){
                    formas.add(new PolilinhaAberta(pontos, cor_escolhida));
                }
                pontos.clear();
                break;
            case KeyEvent.VK_ESCAPE:
                mudarModo(Modo.Idle);
                break;
            case KeyEvent.VK_C:
                escolherCor();
                break;
            case KeyEvent.VK_P:
                escolherQtdLados();
                break;
            case KeyEvent.VK_E:
                modo = Modo.Escalar;
                menuEditar();
                break;
            case KeyEvent.VK_R:
                modo = Modo.Rotacionar;
                menuEditar();
                break;
            case KeyEvent.VK_T:
                modo = Modo.Transladar;
                menuEditar();
                break;
            case KeyEvent.VK_G:
                grade = !(grade);
                break;
            default:
                break;
        }
        
        revalidate();
        repaint();
        
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }
    
    public void tarefasModo(){
        switch(modo){
            case Idle:{
                break;
            }
            case Linha:{
                if (pontos.size() <= 2)
                    pontos.add(new Ponto3D(display[x_clicado][y_clicado].x, display[x_clicado][y_clicado].y));
                if (pontos.size() == 2){
                    formas.add(new Linha(pontos.get(0), pontos.get(1), cor_escolhida));
                    pontos.clear();
                }
                break;
            }
            case Retangulo:{
                if (pontos.size() < 2)
                    pontos.add(new Ponto3D(display[x_clicado][y_clicado].x, display[x_clicado][y_clicado].y));
                if (pontos.size() == 2){
                    formas.add(new Retangulo(pontos.get(0), pontos.get(1), cor_escolhida));
                    pontos.clear();
                }
                break;
            }
            case Elipse:{
                pontos.add(new Ponto3D(display[x_clicado][y_clicado].x, display[x_clicado][y_clicado].y));
                if (pontos.size() == 3){
                    formas.add(new Elipse(pontos.get(0), Funcao.distancia(pontos.get(0), pontos.get(1)), Funcao.distancia(pontos.get(0), pontos.get(2)), cor_escolhida));
                    
                    pontos.clear();
                }
                
                break;
            }
            case Circulo:{
                pontos.add(new Ponto3D(display[x_clicado][y_clicado].x, display[x_clicado][y_clicado].y));
                if (pontos.size() == 2){
                    formas.add(new Elipse(pontos.get(0), Funcao.distancia(pontos.get(0), pontos.get(1)), Funcao.distancia(pontos.get(0), pontos.get(1)), cor_escolhida));
                    
                    pontos.clear();
                }
                
                break;
            }
            case Poligono:{
                pontos.add(new Ponto3D(display[x_clicado][y_clicado].x, display[x_clicado][y_clicado].y));
                if (pontos.size() == 2){
                    formas.add(new Poligono(pontos.get(0), lados_poligono, Funcao.distancia(pontos.get(0), pontos.get(1)), cor_escolhida));
                    
                    pontos.clear();
                }
                break;
            }
            case Polilinha:{
                pontos.add(new Ponto3D(display[x_clicado][y_clicado].x, display[x_clicado][y_clicado].y));
                if (pontos.size() > 1){
                    if (!recortado) Funcao.desenhaLinha(pontos.get(pontos.size()-2), pontos.get(pontos.size()-1), cor_escolhida);
                }
                break;
            }
            case PolilinhaAberta:{
                pontos.add(new Ponto3D(display[x_clicado][y_clicado].x, display[x_clicado][y_clicado].y));
                if (pontos.size() > 1){
                    if (!recortado) Funcao.desenhaLinha(pontos.get(pontos.size()-2), pontos.get(pontos.size()-1), cor_escolhida);
                }
                break;
            }
            case Preencher:{
                Funcao.preencherRecursivo(display[x_clicado][y_clicado].x, display[x_clicado][y_clicado].y, cor_escolhida, display[x_clicado][y_clicado].cor);
                break;
            }
            case Recorte:{
                if (!recortado){
                    if (pontos.size() < 2)
                        pontos.add(new Ponto3D(display[x_clicado][y_clicado].x, display[x_clicado][y_clicado].y));
                    if (pontos.size() == 2){
                        recorte = new Poligono(pontos.get(0), lados_poligono, Funcao.distancia(pontos.get(0), pontos.get(1)), cor_escolhida);
                        pontos.clear();
                        recortado = true;
                        mudarModo(Modo.Idle);
                        carregarPixels();
                    }
                    break;
                }
            }
            case Forma3D:{
                menuForma3D();
                break;
            }
            default:{
                menuEditar();
                break;
            }
        }
        
    }
    
    public void mudarModo(Modo modo) {
        if (this.modo != Modo.Idle && this.modo != modo){
            pontos.clear();
        }
        this.modo = modo;
        
        switch (this.modo){
            case Recorte:
                if (recortado){
                    carregarPixels();
                    recortado = false;
                    this.modo = Modo.Idle;
                }
                break;
            case Remover:
            case Rotacionar:
            case Transladar:
            case Escalar:
            case Projecao:
            case ProjecaoIsometrica:
            case Projecao2Pontos:
            case Projecao3Pontos:
            case ProjecaoObliqua:
                menuEditar();
                break;
            case Forma3D:
                menuForma3D();
                break;
        }
        
        revalidate();
        repaint();
    }
    
    private void escolherQtdLados() {
        JPanel p = new JPanel();
        int n;
        // para impedir valores negativos e 0 nos lados
        SpinnerModel modelo_lados = new SpinnerNumberModel(lados_poligono, 3, 1000, 1);
        
        JSpinner lados_spinner = new JSpinner(modelo_lados);
        
        Dimension d = lados_spinner.getPreferredSize();  
        d.width = 35;

        lados_spinner.setPreferredSize(d);

        p.add(new JLabel("Insira a quantidade de lados dos próximos polígonos regulares: "));
        p.add(lados_spinner);
        
        n = JOptionPane.showConfirmDialog(this, p, "Lados do polígono", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (n == 0){
            lados_poligono = (int) lados_spinner.getValue();
        }
    }
    
    public Forma selecionaForma(){
        if (!formas.isEmpty()){
            Object[] options = formas.toArray();
            Forma n;
            n = (Forma) JOptionPane.showInputDialog(this,
                "Escolha uma forma", "Selecionar Forma",
                JOptionPane.PLAIN_MESSAGE,
                null,
                options, options[0]
                );
            return n; 
        }
        return null;
    }
    
    public void salvar(){
        BufferedImage bImg = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
        Graphics cg = bImg.createGraphics();
        
        paintAll(cg);
        try {
            if (ImageIO.write(bImg, "png", new File("./tela.png")))
            {
                JOptionPane.showMessageDialog(this, "Imagem ./tela.png salva com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(this, "Algo de errado não está certo.", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void menuEscalar(Forma f){
        JPanel p = new JPanel();
        double eixox, eixoy, eixoz, n;
        JSpinner eixox_spinner = new JSpinner(new SpinnerNumberModel(1.0,-1000.0 ,1000.0,0.1));
        JSpinner eixoy_spinner = new JSpinner(new SpinnerNumberModel(1.0,-1000.0 ,1000.0,0.1));
        JSpinner eixoz_spinner = new JSpinner(new SpinnerNumberModel(1.0,-1000.0 ,1000.0,0.1));

        Dimension d = eixox_spinner.getPreferredSize();  
        d.width = 45;
        eixox_spinner.setPreferredSize(d);
        eixoy_spinner.setPreferredSize(d);
        eixoz_spinner.setPreferredSize(d);

        p.add(new JLabel("Eixo X:"));
        p.add(eixox_spinner);
        p.add(new JLabel("      Eixo Y:"));
        p.add(eixoy_spinner);
        p.add(new JLabel("      Eixo Z:"));
        p.add(eixoz_spinner);

        n = JOptionPane.showConfirmDialog(this, p, "Adicione os valores de escala nos eixos", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (n == 0){
            eixox = (double) eixox_spinner.getValue();
            eixoy = (double) eixoy_spinner.getValue();
            eixoz = (double) eixoz_spinner.getValue();

            f.ajustar(Matriz.escalar(f.getMatriz(), eixox, eixoy, eixoz));

            carregarPixels();
        }
    }
    
    private void menuRotacionar(Forma f){
        JPanel p = new JPanel();
        int angulo, eixo, n = 0;
        JSpinner angulo_spinner = new JSpinner();

        String [] lista = { "X", "Y", "Z" };
        JComboBox lista_eixos = new JComboBox(lista);
        
        lista_eixos.setSelectedIndex(0);

        Dimension d = angulo_spinner.getPreferredSize();  
        d.width = 45;
        angulo_spinner.setPreferredSize(d);

        p.add(new JLabel("Ângulo:"));
        p.add(angulo_spinner);
        p.add(new JLabel("º     Eixo:"));
        p.add(lista_eixos);

        n = JOptionPane.showConfirmDialog(this, p, "Adicione os valores de rotação nos eixos", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (n == 0){
            angulo = (int) angulo_spinner.getValue();
            eixo = lista_eixos.getSelectedIndex();

            f.ajustar(Matriz.rotacionar(f.getMatriz(), angulo, eixo));

            carregarPixels();
        }
    }
    
    private void menuTransladar(Forma f){
        JPanel p = new JPanel();
        int eixox, eixoy, eixoz, n;
        JSpinner eixox_spinner = new JSpinner();
        JSpinner eixoy_spinner = new JSpinner();
        JSpinner eixoz_spinner = new JSpinner();

        Dimension d = eixox_spinner.getPreferredSize();  
        d.width = 35;
        eixox_spinner.setPreferredSize(d);
        eixoy_spinner.setPreferredSize(d);
        eixoz_spinner.setPreferredSize(d);

        p.add(new JLabel("Eixo X:"));
        p.add(eixox_spinner);
        p.add(new JLabel("      Eixo Y:"));
        p.add(eixoy_spinner);
        p.add(new JLabel("      Eixo Z:"));
        p.add(eixoz_spinner);

        n = JOptionPane.showConfirmDialog(this, p, "Adicione os valores de translação nos eixos", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (n == 0){
            eixox = (int) eixox_spinner.getValue();
            eixoy = (int) eixoy_spinner.getValue();
            eixoz = (int) eixoz_spinner.getValue();

            f.ajustar(Matriz.transladar(f.getMatriz(), eixox, eixoy, eixoz));

            carregarPixels();
        }
    }
    

    public void menuEditar(){
        if (!formas.isEmpty()){
            Forma n = selecionaForma();
            if (n != null){
                switch(modo){
                    case Remover:{
                        formas.remove(n);
                        break;
                    }
                    case Transladar:{
                        menuTransladar(n);
                        break;
                    }
                    case Rotacionar:{
                        menuRotacionar(n);
                        break;
                    }
                    case Escalar:{
                        menuEscalar(n);
                        break;
                    }
                    case Projecao:{
                        menuProjecao(n);
                        break;
                    }
                    case ProjecaoObliqua:{
                        menuProjecaoObliqua(n);
                        break;
                    }
                    case Projecao2Pontos:{
                        menuProjecao2Pontos(n);
                        break;
                    }
                    case Projecao3Pontos:{
                        menuProjecao3Pontos(n);
                        break;
                    }
                    case ProjecaoIsometrica:{
                        menuProjecaoIsometrica(n);
                        break;
                    }
                }
                carregarPixels();
            }
        }
        else{
            JOptionPane.showMessageDialog(this, "Não existe nenhuma forma!", "Erro", JOptionPane.ERROR_MESSAGE);
            modo = Modo.Idle;
        }
    }
    
    public void menuProjecao(Forma f){
        JPanel p = new JPanel();
        int distancia, n = 0;
        JSpinner distancia_spinner = new JSpinner();

        Dimension d = distancia_spinner.getPreferredSize();  
        d.width = 45;
        distancia_spinner.setPreferredSize(d);

        p.add(new JLabel("Distância D:"));
        p.add(distancia_spinner);

        n = JOptionPane.showConfirmDialog(this, p, "Adicione os valores pra projeção", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (n == 0){
            distancia = (int) distancia_spinner.getValue();
            boolean flag = false;
            
            for (Ponto3D w: f.getPontos()){
                if (w.z < distancia){
                    JOptionPane.showMessageDialog(this, "O seguinte ponto possui Z menor que a distância definida: " + w, "Erro", JOptionPane.ERROR_MESSAGE);
                    flag = true;
                    break;
                }
            }
            if (!flag) f.ajustar(Matriz.projecao(f.getMatriz(), distancia));

            carregarPixels();
        }
    }
    
    public void menuProjecaoObliqua(Forma f){
        JPanel p = new JPanel();
        int angulo, n = 0;
        double delta;
        JSpinner angulo_spinner = new JSpinner();

        String [] lista = { "Cavalier", "Cabinet"};
        JComboBox lista_projecoes = new JComboBox(lista);
        
        lista_projecoes.setSelectedIndex(0);

        Dimension d = angulo_spinner.getPreferredSize();  
        d.width = 45;
        angulo_spinner.setPreferredSize(d);

        p.add(new JLabel("Ângulo:"));
        p.add(angulo_spinner);
        p.add(new JLabel("º     Delta:"));
        p.add(lista_projecoes);

        n = JOptionPane.showConfirmDialog(this, p, "Adicione os valores pra projeção", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (n == 0){
            angulo = (int) angulo_spinner.getValue();
            delta = (lista_projecoes.getSelectedIndex() == 0)? 1 : 0.5;

            f.ajustar(Matriz.projecaoObliqua(f.getMatriz(), delta, angulo));

            carregarPixels();
        }
    }
    
    public void menuProjecao2Pontos(Forma f){
        JPanel p = new JPanel();
        int angulo, distancia, eixo, n = 0;
        JSpinner angulo_spinner = new JSpinner();
        JSpinner distancia_spinner = new JSpinner();

        String [] lista = {"X", "Y"};
        JComboBox lista_eixos = new JComboBox(lista);
        
        lista_eixos.setSelectedIndex(0);

        Dimension d = angulo_spinner.getPreferredSize();  
        d.width = 45;
        angulo_spinner.setPreferredSize(d);
        distancia_spinner.setPreferredSize(d);
        
        p.add(new JLabel("Distância D:"));
        p.add(distancia_spinner);
        p.add(new JLabel("      Ângulo:"));
        p.add(angulo_spinner);
        p.add(new JLabel("º     Eixo:"));
        p.add(lista_eixos);

        n = JOptionPane.showConfirmDialog(this, p, "Adicione os valores pra projeção", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (n == 0){
            angulo = (int) angulo_spinner.getValue();
            eixo = lista_eixos.getSelectedIndex();
            distancia = (int) distancia_spinner.getValue();
            boolean flag = false;
            
            for (Ponto3D w: f.getPontos()){
                if (w.z < distancia){
                    JOptionPane.showMessageDialog(this, "O seguinte ponto possui Z menor que a distância definida: " + w, "Erro", JOptionPane.ERROR_MESSAGE);
                    flag = true;
                    break;
                }
            }
            if (!flag) f.ajustar(Matriz.projecao2Pontos(f.getMatriz(), distancia, angulo, eixo));
            carregarPixels();
        }
    }
    
    public void menuProjecao3Pontos(Forma f) {
        JPanel p = new JPanel();
        int angulo1, distancia, angulo2, n;
        JSpinner angulo1_spinner = new JSpinner();
        JSpinner angulo2_spinner = new JSpinner();
        JSpinner distancia_spinner = new JSpinner();

        Dimension d = angulo1_spinner.getPreferredSize();  
        d.width = 45;
        angulo1_spinner.setPreferredSize(d);
        angulo2_spinner.setPreferredSize(d);
        distancia_spinner.setPreferredSize(d);
        
        p.add(new JLabel("Distância D:"));
        p.add(distancia_spinner);
        p.add(new JLabel("      Ângulo no eixo X:"));
        p.add(angulo1_spinner);
        p.add(new JLabel("º     Ângulo no eixo Y:"));
        p.add(angulo2_spinner);
        p.add(new JLabel("º"));

        n = JOptionPane.showConfirmDialog(this, p, "Adicione os valores pra projeção", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (n == 0){
            angulo1 = (int) angulo1_spinner.getValue();
            angulo2 = (int) angulo2_spinner.getValue();
            distancia = (int) distancia_spinner.getValue();
            boolean flag = false;
            
            for (Ponto3D w: f.getPontos()){
                if (w.z < distancia){
                    JOptionPane.showMessageDialog(this, "O seguinte ponto possui Z menor que a distância definida: " + w, "Erro", JOptionPane.ERROR_MESSAGE);
                    flag = true;
                    break;
                }
            }
            if (!flag) f.ajustar(Matriz.projecao3Pontos(f.getMatriz(), distancia, angulo1, angulo2));
            carregarPixels();
        }
    }

    public void menuProjecaoIsometrica(Forma f) {
        f.ajustar(Matriz.projecaoIsometrica(f.getMatriz()));
        carregarPixels();
    }
    
    public void menuForma3D(){
        
        JPanel p;
        int n;
        Dimension d;
        String[] texto_matriz;
        String [] texto_vetor;
        int [] resultados;
        ArrayList<ArrayList<Integer>> matriz = new ArrayList<>();

        p = new JPanel(new GridLayout(0,1));
        JTextField entrada = new JTextField();

        d = entrada.getPreferredSize();
        d.width = 100;
        entrada.setPreferredSize(d);

        p.add(new JLabel("Adicione os pontos. Ex:0 0 0,2 0 0,2 2 0"));
        p.add(entrada);


        n = JOptionPane.showConfirmDialog(this, p, "Adicione os pontos", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

        if (n == 0){

            texto_matriz = entrada.getText().split(",");
            if (!texto_matriz[0].isEmpty()){
                try{
                    for (String vetor : texto_matriz) {
                        texto_vetor = vetor.split(" ");
                        resultados = new int[texto_vetor.length];
                        for (int j = 0; j < texto_vetor.length; j++) {
                            resultados[j] = Integer.parseInt(texto_vetor[j]);
                        }
                        pontos.add(new Ponto3D(resultados[0],resultados[1],resultados[2]));
                        adicionaPonto(resultados[0],resultados[1],cor_escolhida);
                    }
                }
                catch(java.lang.NumberFormatException e){
                    JOptionPane.showMessageDialog(this, "Forma 3D não criada.", "Erro", JOptionPane.ERROR_MESSAGE);
                }         
            }
        }

        if (pontos.size() < 2){
            JOptionPane.showMessageDialog(this, "Forma 3D não criada.", "Erro", JOptionPane.ERROR_MESSAGE);
        }
        else{
            
            p = new JPanel(new GridLayout(0,1));
            entrada = new JTextField();

            d = entrada.getPreferredSize();
            d.width = 100;
            entrada.setPreferredSize(d);
            
            String [] lista = new String[pontos.size()];
            lista[0] = "0 = " + pontos.get(0).toString();
            
            for (int i = 1 ; i < pontos.size() ; i++){
                lista[i] = i + " = " + pontos.get(i).toString();
            }
            
            JList lista_box = new JList(lista);
            lista_box.setEnabled(false);
            JScrollPane scroll = new JScrollPane(lista_box);
            scroll.setPreferredSize(new Dimension(scroll.getPreferredSize().width, 100));

            p.add(new JLabel("Adicione as faces em ordem (Ex:0,1,2,3;4,5,6,7)"));
            p.add(scroll);
            p.add(entrada);
            
            
            n = JOptionPane.showConfirmDialog(this, p, "Crie as faces", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

            if (n == 0){

                texto_matriz = entrada.getText().replaceAll("\\s+","").split(";");
                if (!texto_matriz[0].isEmpty()){
                    try{
                        for (String vetor : texto_matriz) {
                            texto_vetor = vetor.split(",");
                            resultados = new int[texto_vetor.length];
                            for (int j = 0; j < texto_vetor.length; j++) {
                                resultados[j] = Integer.parseInt(texto_vetor[j]);
                            }
                            matriz.add(new ArrayList<>());
                            for (int x: resultados)
                                matriz.get(matriz.size()-1).add(x);
                        }
                        formas.add(new Forma3D(pontos, matriz, cor_escolhida));
                    }
                    catch(java.lang.NumberFormatException e){
                        JOptionPane.showMessageDialog(this, "Forma 3D não criada.", "Erro", JOptionPane.ERROR_MESSAGE);
                    }         
                }
            }
        }
        pontos.clear();
        carregarPixels();
    }
    
    public void manual() {
        new Manual();
    }

    public void sobre() {
        JOptionPane.showMessageDialog(this, "Universidade Federal do Pará.\nBacharelado em Ciência da Computação.\nDisciplina: Computação Gráfica.\nProf.: Bianchi Serique Meiguins.\nMSc.: Carlos Gustavo Resque dos Santos.\nAluna: Giordanna De Gregoriis.\nGioPaint 1.0.", "Sobre", JOptionPane.INFORMATION_MESSAGE);
    }
}