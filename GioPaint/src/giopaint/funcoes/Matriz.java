package giopaint.funcoes;

public class Matriz {
    
    private static double[][] rotacionar(int grau, int eixo) {
        double [][] matriz = new double[4][4];
        for (int i = 0 ; i < 4 ; i++){
            for (int j = 0 ; j < 4 ; j++){
                matriz[i][j] = 0;
                if (i == j) matriz[i][j] = 1;
            }
        }
        switch (eixo){
            case 0:{ //x
                matriz[1][1] = Math.cos(Math.toRadians(grau));
                matriz[2][1] = Math.sin(Math.toRadians(grau));
                matriz[1][2] = -Math.sin(Math.toRadians(grau));
                matriz[2][2] = Math.cos(Math.toRadians(grau));
                break;
            }
            case 1:{ //y
                matriz[0][0] = Math.cos(Math.toRadians(grau));
                matriz[0][2] = Math.sin(Math.toRadians(grau));
                matriz[2][0] = -Math.sin(Math.toRadians(grau));
                matriz[2][2] = Math.cos(Math.toRadians(grau));
                break;
            }
            case 2:{ //z
                matriz[0][0] = Math.cos(Math.toRadians(grau));
                matriz[1][0] = Math.sin(Math.toRadians(grau));
                matriz[0][1] = -Math.sin(Math.toRadians(grau));
                matriz[1][1] = Math.cos(Math.toRadians(grau));
                break;
            }
        }
        
        return matriz;
    }
    
    private static double[][] transladar(double Tx, double Ty, double Tz){
        double [][] matriz = new double[4][4];
        for (int i = 0 ; i < 4 ; i++){
            for (int j = 0 ; j < 4 ; j++){
                matriz[i][j] = 0;
                if (i == j) matriz[i][j] = 1;
            }
        }
        
        matriz[0][3] = Tx;
        matriz[1][3] = Ty;
        matriz[2][3] = Tz;
        
        return matriz;
    }
    
    private static double[][] escalar(double Sx, double Sy, double Sz){
        double [][] matriz = new double[4][4];
        for (int i = 0 ; i < 4 ; i++)
            for (int j = 0 ; j < 4 ; j++)
                matriz[i][j] = 0;
        matriz[0][0] = Sx;
        matriz[1][1] = Sy;
        matriz[2][2] = Sz;
        matriz[3][3] = 1;
        
        return matriz;
    }
    
    private static double[][] multiplicacao(double [][] primeira, double[][] segunda){
        int linhasA = primeira.length;
        int colunasA = primeira[0].length;
        int linhasB = segunda.length;
        int colunasB = segunda[0].length;
        
        double segundax = 0, segunday = 0, segundaz = 0;
        
        boolean voltaorigem = false;
        
        if (segunda[0][0] != 0 || segunda[1][0] != 0){
            voltaorigem = true;
            
            segundax = segunda[0][0];
            segunday = segunda[1][0];
            segundaz = segunda[1][0];
            
            segunda = multi(transladar(-segundax, -segunday, -segundaz), segunda);
        }

        if (colunasA == linhasB){
            double[][] produto = new double[linhasA][colunasB];
            for (int i = 0; i < linhasA; i++)
                for (int j = 0; j < colunasB; j++)
                    produto[i][j] = 0;
            
            for (int i = 0; i < linhasA; i++) {
                for (int j = 0; j < colunasB; j++) {
                    for (int k = 0; k < colunasA; k++) {
                        produto[i][j] += primeira[i][k] * segunda[k][j];
                    }
                    produto[i][j] = Math.round(produto[i][j]);
                }
            }
            
            if (voltaorigem){
                produto = multi(transladar(segundax, segunday, segundaz), produto);
            }
            
            return produto;
        }
        else{
            return null;
        }
    }
    
    private static double[][] multi(double [][] primeira, double[][] segunda){
        int linhasA = primeira.length;
        int colunasA = primeira[0].length;
        int linhasB = segunda.length;
        int colunasB = segunda[0].length;

        if (colunasA == linhasB){
            double[][] produto = new double[linhasA][colunasB];
            for (int i = 0; i < linhasA; i++)
                for (int j = 0; j < colunasB; j++)
                    produto[i][j] = 0;
            
            for (int i = 0; i < linhasA; i++) {
                for (int j = 0; j < colunasB; j++) {
                    for (int k = 0; k < colunasA; k++) {
                        produto[i][j] += primeira[i][k] * segunda[k][j];
                    }
                }
            }
            return produto;
        }
        else{
            return null;
        }
    }
    
    private static void imprimeMatriz(double[][] matriz){
        for (int j = 0 ; j < 4 ; j++) {
            for (int i = 1; i < matriz[0].length ; i ++) {
                System.out.print( "\t" + (int) matriz[j][i]);
            }
            System.out.println();
        }
        System.out.println();
    }
    
    private static double[][] projecao(double d){
        double [][] matriz = new double[4][4];
        for (int i = 0 ; i < 4 ; i++)
            for (int j = 0 ; j < 4 ; j++)
                matriz[i][j] = 0;
        
        matriz[0][0] = d;
        matriz[1][1] = d;
        matriz[2][2] = d;
        matriz[3][2] = 1;
        
        return matriz;
    }

    private static double[][] projecaoObliqua(double delta, double angulo){
        double [][] matriz = new double[4][4];
        for (int i = 0 ; i < 4 ; i++)
            for (int j = 0 ; j < 4 ; j++)
                matriz[i][j] = 0;
        matriz[0][0] = 1;
        matriz[1][1] = 1;
        matriz[3][3] = 1;
        matriz[0][2] = delta * Math.cos(Math.toRadians(angulo));
        matriz[1][2] = delta * Math.cos(Math.toRadians(angulo));
        
        return matriz;
    }
    
    // métodos sobrecarregados
    public static double[][] projecao(double [][] matriz, double d){
        
        double [][] nova = multiplicacao(projecao(d), matriz);
        // corrigindo coordenada homogenea
        for (int i = 0 ; i < nova[0].length ; i++){
            for (int j = 0 ; j < 4 ; j++){
                nova[j][i] /= nova [3][i];
            }
        }
        
        return nova;
    }
    
    public static double[][] projecaoIsometrica(double [][] matriz){
        return rotacionar(rotacionar(matriz, 45, 1), 45, 0);
    }
    
    public static double[][] projecao2Pontos(double [][] matriz, int d, int angulo, int eixo){
        return projecao(transladar(rotacionar(matriz, angulo, eixo), 0, 0, d), d);
    }
    
    public static double[][] projecao3Pontos(double [][] matriz, int d, int angulo1, int angulo2){
        return projecao(transladar(rotacionar(rotacionar(matriz, angulo2, 1), angulo1, 0), 0, 0, d), d);
    }
    
    public static double[][] projecaoObliqua(double [][] matriz, double lambda, double angulo){
        return multiplicacao(projecaoObliqua(lambda, angulo), matriz);
    }
    
    public static double[][] escalar(double [][] matriz, double Sx, double Sy, double Sz){
        return multiplicacao(escalar(Sx, Sy, Sx), matriz);
    }
    
    public static double[][] rotacionar(double [][] matriz, int grau, int eixo) {
        return multiplicacao(rotacionar(grau, eixo), matriz);
    }
    
    public static double[][] transladar(double [][] matriz, double Tx, double Ty, double Tz){
        return multiplicacao(transladar(Tx, Ty, Tz), matriz);
    }
}
