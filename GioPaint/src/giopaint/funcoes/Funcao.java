package giopaint.funcoes;

import giopaint.Display;
import giopaint.Pixel;
import giopaint.Ponto3D;
import giopaint.formas.*;
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;

public class Funcao {
    
    private static boolean trocax, trocay, reflete;
    
    public static void recorte(Poligono recorte){
        for (Forma x: Display.instancia().getFormas()){
            try{
                recortePoligono(x, recorte).desenha();
            }
            catch (java.lang.NullPointerException e){}
        }
    }
    
    private static Forma recortePoligono(Forma forma, Poligono recorte) {
        
        if (forma instanceof Elipse) return null;
        
        ArrayList<Ponto3D> resultado = new ArrayList<>(Arrays.asList(forma.getPontos()));
        ArrayList<Ponto3D> clipper = new ArrayList<>(Arrays.asList(recorte.getPontos()));
        ArrayList<Ponto3D> input;

        int lados_recorte = clipper.size();
        for (int i = 0; i < lados_recorte; i++) {
 
            int lados_resultado = resultado.size();
            input = new ArrayList<>(resultado);
            resultado = new ArrayList<>(lados_resultado);
 
            Ponto3D A = clipper.get((i + lados_recorte - 1) % lados_recorte);
            Ponto3D B = clipper.get(i);
 
            for (int j = 0; j < lados_resultado; j++) {
 
                Ponto3D P = input.get((j + lados_resultado - 1) % lados_resultado);
                Ponto3D Q = input.get(j);
 
                if (estaDentro(A, B, Q)) {
                    if (!estaDentro(A, B, P)){
                        resultado.add(intersecao(A, B, P, Q));
                    }
                    resultado.add(Q);
                } else if (estaDentro(A, B, P)){
                    resultado.add(intersecao(A, B, P, Q));
                }
            }
        }
        if (forma instanceof Linha) return new Linha(resultado.get(0), resultado.get(1), forma.cor);
        else if (forma instanceof PolilinhaAberta) return new PolilinhaAberta(resultado, forma.cor);
        return new Polilinha(resultado, forma.cor);
    }
 
    private static boolean estaDentro(Ponto3D a, Ponto3D b, Ponto3D c) {
        return (a.x - c.x) * (b.y - c.y) > (a.y - c.y) * (b.x - c.x);
    }
 
    private static Ponto3D intersecao(Ponto3D a, Ponto3D b, Ponto3D p, Ponto3D q) {
        double A1 = b.y - a.y;
        double B1 = a.x - b.x;
        double C1 = A1 * a.x + B1 * a.y;
 
        double A2 = q.y - p.y;
        double B2 = p.x - q.x;
        double C2 = A2 * p.x + B2 * p.y;
 
        double det = A1 * B2 - A2 * B1;
        double x = (B2 * C1 - B1 * C2) / det;
        double y = (A1 * C2 - A2 * C1) / det;
        
        int dx, dy;
        dx = (int) (x > 0 ? Math.round(x) : Math.floor(x));
        dy = (int) (y > 0 ? Math.round(y) : Math.floor(y));
        
        //tentando corrigir a conversão pra inteiro e manter as linhas originais
        if (x < 0 && y < 0){
            dx = (int) Math.round(x);
            dy = (int) Math.round(y);
        }
        return new Ponto3D(dx, dy);
    }

    public static void preencherRecursivo(int x, int y, Color cor, Color meio){
        Color atual = Display.instancia().lerPixel(x, y);
        try{
            if (atual.equals(meio) && !atual.equals(cor) && !atual.equals(null)){
                Display.instancia().adicionaPonto(x, y, cor);
                preencherRecursivo(x+1, y, cor, meio);
                preencherRecursivo(x, y+1, cor, meio);
                preencherRecursivo(x-1, y, cor, meio);
                preencherRecursivo(x, y-1, cor, meio);
            }
        }
        catch (Exception e){
        }
    }
    
    public static int distancia(Ponto3D p1, Ponto3D p2){
        return (int) Math.sqrt( Math.pow( p1.x - p2.x, 2) + Math.pow( p1.y - p2.y, 2) );
    }
    
    public static void desenhaLinha(Ponto3D p1, Ponto3D p2, Color cor) {
        
        int deltax, deltay, x, y, i;
        double m, e;

        // cópia manual que assim evita dos pontos originais serem modificados
        Point[] pontos = new Point[2];
        pontos[0] = new Point(p1.x, p1.y);
        pontos[1] = new Point(p2.x, p2.y);

        verificaOctante(pontos); // aplica reflexão;

        Pixel[] pontos_pintados = new Pixel[(int) (pontos[1].x - pontos[0].x) + 1];

        x = pontos[0].x;
        y = pontos[0].y;

        deltax = Math.abs(pontos[1].x - pontos[0].x); // atualiza o delta x
        deltay = Math.abs(pontos[1].y - pontos[0].y); // atualiza o delta y

        m = deltay / (deltax * 1.0);
        e = m - 0.5;

        pontos_pintados[0] = new Pixel(pontos[0], cor);

        for (i = 1; i <= deltax; i++) {

            if (e >= 0) {
                y++;
                e--;
            }
            x++;
            e += m;

            pontos_pintados[i] = new Pixel(x, y, cor);

        }

        reflexaoInv(pontos_pintados);
        Display.instancia().adicionaPontos(pontos_pintados);
        
    }

    //verifica em qual octante está e vê quais flags vão ser acionadas
    private static void verificaOctante(Point[] pontos) {
        int deltax, deltay;
        
        deltax = pontos[1].x - pontos[0].x; // delta x
        deltay = pontos[1].y - pontos[0].y; // delta y
        
        if (deltay >= 0){ // y tá crescendo ou parado
            trocay = false;
            if (deltax >= 0){ // x tá crescendo ou parado
                trocax = false;
                reflete = Math.abs(deltax) < Math.abs(deltay);
                // se verdadeiro: octante 2, senão: octante 1
            }
            else{ // x tá diminuindo
                trocax = true;
                reflete = Math.abs(deltax) < Math.abs(deltay);
                // se verdadeiro: octante 3, senão: octante 4
            }
        }
        else{ // y tá diminuindo
            trocay = true;
            if (deltax >= 0){ // x tá subindo ou parado
                trocax = false;
                reflete = Math.abs(deltax) < Math.abs(deltay);
                // se verdadeiro: octante 7, senão: octante 8
            }
            else{ // x tá diminuindo
                trocax = true;
                reflete = Math.abs(deltax) < Math.abs(deltay);
                // se verdadeiro: octante 6, senão: octante 5
            }
        }
        reflexao(pontos);

    }

    // reflexão
    private static void reflexao(Point[] pontos) {
        
        if(trocay){
            for (Point p : pontos) {
                p.setLocation(p.getX(), -p.getY());
            }
        }
        if(trocax){
            for (Point p : pontos) {
                p.setLocation(-p.getX(), p.getY());
            }
        }
        if(reflete){
            for (Point p : pontos) {
                p.setLocation(p.getY(), p.getX());
            }
        }
    }
    
    // reflexão inversa
    private static void reflexaoInv(Point[] pontos) {

        if(reflete){
            for (Point p : pontos) {
                p.setLocation(p.getY(), p.getX());
            }
        }
        if(trocay){
            for (Point p : pontos) {
                p.setLocation(p.getX(), -p.getY());
            }
        }
        if(trocax){
            for (Point p : pontos) {
                p.setLocation(-p.getX(), p.getY());
            }
        }
    }
    
}
