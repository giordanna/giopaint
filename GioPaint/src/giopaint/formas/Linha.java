package giopaint.formas;

import giopaint.Ponto3D;
import giopaint.funcoes.Funcao;
import java.awt.Color;

public class Linha extends Forma {
    public Ponto3D ponto1, ponto2;
    
    public Linha(){
        super();
        ponto1 = new Ponto3D();
        ponto2 = new Ponto3D();
    }
    
    public Linha(Ponto3D p1, Ponto3D p2){
        super();
        ponto1 = new Ponto3D(p1);
        ponto2 = new Ponto3D(p2);
    }
    
    public Linha(Ponto3D p1, Ponto3D p2, Color cor){
        super(cor);
        ponto1 = new Ponto3D(p1);
        ponto2 = new Ponto3D(p2);
    }
    
    public Linha(int x1, int y1, int x2, int y2){
        super();
        ponto1 = new Ponto3D(x1, y1);
        ponto2 = new Ponto3D(x2, y2);
    }
    
    public Linha(int x1, int y1, int x2, int y2, Color cor){
        super(cor);
        ponto1 = new Ponto3D(x1, y1);
        ponto2 = new Ponto3D(x2, y2);
    }

    @Override
    public void desenha() {
        Funcao.desenhaLinha(ponto1, ponto2, cor);
    }
    
    @Override
    public void ajustar(double [][] matriz){
        ponto1.x = (int) matriz[0][0];
        ponto1.y = (int) matriz[1][0];
        ponto1.z = (int) matriz[2][0];
        
        ponto2.x = (int) matriz[0][1];
        ponto2.y = (int) matriz[1][1];
        ponto1.z = (int) matriz[2][1];
    }
    
    @Override
    public double[][] getMatriz(){
        double [][] matriz = new double[4][2];
        for (int i = 0 ; i < 2 ; i++){
            matriz[3][i] = 1; // não é o eixo z mas deve ficar tudo um
        }
        
        matriz[0][0] = ponto1.x;
        matriz[1][0] = ponto1.y;
        matriz[2][0] = ponto1.z;
        
        matriz[0][1] = ponto2.x;
        matriz[1][1] = ponto2.y;
        matriz[2][1] = ponto2.z;
        
        return matriz;
    }
    
    @Override
    public Ponto3D[] getPontos() {
        return new Ponto3D[]{ponto1, ponto2};
    }
    
    @Override
    public String toString(){
        return "Linha | " + super.toString() + " | ponto 1=" + ponto1 + " | ponto 2=" + ponto2;
    }
}
