package giopaint.formas;

import giopaint.Ponto3D;
import giopaint.funcoes.Funcao;
import java.awt.Color;
import java.util.ArrayList;

public class Polilinha extends Forma {
    public Ponto3D [] pontos;
    
    public Polilinha(){
        super();
        pontos = new Ponto3D[2];
    }
    
    public Polilinha(Ponto3D[] pontos){
        super();
        this.pontos = new Ponto3D[pontos.length];
        this.pontos = pontos.clone();
    }
    
    public Polilinha(ArrayList<Ponto3D> pontos){
        super();
        this.pontos = new Ponto3D[pontos.size()];
        this.pontos = (Ponto3D[]) pontos.toArray();
    }
    
    public Polilinha(Ponto3D [] pontos, Color cor){
        super(cor);
        this.pontos = new Ponto3D[pontos.length];
        this.pontos = pontos.clone();
    }
    
    public Polilinha(ArrayList<Ponto3D> pontos, Color cor){
        super(cor);
        this.pontos = new Ponto3D[pontos.size()];
        this.pontos = pontos.toArray(this.pontos);
    }
    
    @Override
    public void desenha() {
        for (int i = 0 ; i < pontos.length ; i++){
            Funcao.desenhaLinha(pontos[i], pontos[(i+1) % pontos.length], cor);
        }
    }
    
    @Override
    public void ajustar(double [][] matriz){
        
        for (int i = 1 ; i < pontos.length + 1; i++){
            pontos[i - 1].x = (int) matriz[0][i];
            pontos[i - 1].y = (int) matriz[1][i];
            pontos[i - 1].z = (int) matriz[2][i];
        }
    }
    
    @Override
    public double[][] getMatriz(){
        double [][] matriz = new double[4][pontos.length + 1];
        
        Ponto3D c = centroide(pontos);
        matriz[0][0] = c.x;
        matriz[1][0] = c.y;
        matriz[2][0] = c.z;
        matriz[3][0] = 1;
        
        for (int i = 1 ; i < pontos.length + 1 ; i++){
            matriz[0][i] = pontos[i - 1].x;
            matriz[1][i] = pontos[i - 1].y;
            matriz[2][i] = pontos[i - 1].z;
            matriz[3][i] = 1;
        }
        
        return matriz;
    }
    
    @Override
    public Ponto3D[] getPontos() {
        return pontos;
    }
    
    @Override
    public String toString(){
        return "Polígono fechado | " + super.toString() + " | Qtd. pontos=" + pontos.length;
    }
}
