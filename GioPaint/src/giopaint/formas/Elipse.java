package giopaint.formas;

import giopaint.Display;
import giopaint.Ponto3D;
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

public class Elipse extends Forma {
    public Ponto3D meio;
    public int raiox, raioy;
    
    public Elipse(){
        super();
        meio = new Ponto3D();
        raiox = 1;
        raioy = 1;
    }
    
    public Elipse(Ponto3D meio, int raiox, int raioy){
        super();
        this.meio = new Ponto3D(meio);
        this.raiox = raiox;
        this.raioy = raioy;
    }
    
    public Elipse(int x, int y, int raiox, int raioy){
        super();
        meio = new Ponto3D(x, y);
        this.raiox = raiox;
        this.raioy = raioy;
        
    }
    
    public Elipse(Ponto3D meio, int raiox, int raioy, Color cor){
        super(cor);
        this.meio = new Ponto3D(meio);
        this.raiox = raiox;
        this.raioy = raioy;
    }
    
    public Elipse(int x, int y, int raiox, int raioy, Color cor){
        super(cor);
        meio = new Ponto3D(x, y);
        this.raiox = raiox;
        this.raioy = raioy;
    }
    
    @Override
    public void desenha() {
        if (raiox == raioy){
            desenhaCirculo();
        }
        else{
            Point ponto = new Point(meio.x, meio.y);
            int raioxsq = raiox * raiox;
            int raioysq = raioy * raioy;
            int x = 0, y = raioy;

            instrucaoElipse(x, y, cor);
            ponto.x = 0; ponto.y = 2 * raioxsq * y;

            double p = raioysq - (raioxsq * raioy) + (0.25 * raioxsq);

            do{
                x++;
                ponto.x = ponto.x + 2 * raioysq;
                if (p < 0){
                    p += raioysq + ponto.x;
                }
                else{
                    y--;
                    ponto.y = ponto.y - 2 * raioxsq;
                    p = p + raioysq + ponto.x - ponto.y;
                }
                instrucaoElipse(x, y, cor);
            } while (ponto.x < ponto.y);

            p = raioysq * (x + 0.5) * (x + 0.5) + raioxsq * (y - 1) * (y - 1) - raioxsq * raioysq;
            do{
                y--;
                ponto.y = ponto.y - 2* raioxsq;
                if (p > 0){
                    p += raioxsq - ponto.y;
                }
                else{
                    x++;
                    ponto.x = ponto.x + 2 * raioysq;
                    p += raioxsq - ponto.y + ponto.x;
                }
                instrucaoElipse(x, y, cor);
            } while (y > 0);
        }
    }
    
    public void instrucaoElipse(int x, int y, Color cor){
        Display.instancia().adicionaPonto(meio.x + x, meio.y + y, cor);
        Display.instancia().adicionaPonto(meio.x - x, meio.y + y, cor);
        Display.instancia().adicionaPonto(meio.x + x, meio.y - y, cor);
        Display.instancia().adicionaPonto(meio.x - x, meio.y - y, cor);
    }
    
    public void desenhaCirculo(){
        ArrayList<Point> pontos_circulo = new ArrayList<>();        
        int x = 0, y = raiox, p = 1 - raiox;

        pontos_circulo.add(new Point(x, y));
        while ( x < y ){ 
            x++;
            if ( p < 0 ){
                p += 2 * x + 3;
            }
            else{
                y--;
                p += 2 * x - 2 * y + 5;
            }
            pontos_circulo.add(new Point(x, y));
        }
        int i = pontos_circulo.size() - 1;

        Point aux;
        while (i >= 0){
            aux = pontos_circulo.get(i);
            
            Display.instancia().adicionaPonto(aux.x + meio.x, aux.y + meio.y, cor);
            Display.instancia().adicionaPonto(-aux.x + meio.x, aux.y + meio.y, cor);
            Display.instancia().adicionaPonto(aux.x + meio.x, -aux.y + meio.y, cor);
            Display.instancia().adicionaPonto(-aux.x + meio.x, -aux.y + meio.y, cor);
            Display.instancia().adicionaPonto(aux.y + meio.x, aux.x + meio.y, cor);
            Display.instancia().adicionaPonto(-aux.y + meio.x, aux.x + meio.y, cor);
            Display.instancia().adicionaPonto(aux.y + meio.x, -aux.x + meio.y, cor);
            Display.instancia().adicionaPonto(-aux.y + meio.x, -aux.x + meio.y, cor);
            
            i--;
        }
    }

    @Override
    public void ajustar(double [][] matriz){
        
    }
    
    @Override
    public double[][] getMatriz(){
        return null;
    }

    @Override
    public Ponto3D[] getPontos() {
        return null;
    }
    
    @Override
    public String toString(){
        if (raiox == raioy) return "Círculo | " + super.toString() + " | meio=" + meio + " | raio=" + raiox;
        return "Elipse | " + super.toString() + " | meio=" + meio + " | raio x=" + raiox + " | raio y=" + raioy;
    }
}
