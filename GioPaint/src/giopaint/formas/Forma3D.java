package giopaint.formas;

import giopaint.Ponto3D;
import giopaint.funcoes.Funcao;
import java.awt.Color;
import java.util.ArrayList;

public class Forma3D extends Forma {
    public Ponto3D[] pontos;
    public int [][] faces;
    
    public Forma3D(){
        super();
        pontos = new Ponto3D[2];
    }
    
    public Forma3D(Ponto3D[] pontos){
        super();
        this.pontos = new Ponto3D[pontos.length];
        this.pontos = pontos.clone();
    }
    
    public Forma3D(ArrayList<Ponto3D> pontos){
        super();
        this.pontos = new Ponto3D[pontos.size()];
        this.pontos = (Ponto3D[]) pontos.toArray();
    }
    
    public Forma3D(Ponto3D [] pontos, Color cor){
        super(cor);
        this.pontos = new Ponto3D[pontos.length];
        this.pontos = pontos.clone();
    }
    
    public Forma3D(Ponto3D [] pontos, int [][] faces, Color cor){
        super(cor);
        this.pontos = new Ponto3D[pontos.length];
        this.pontos = pontos.clone();

        this.faces = faces.clone();
    }
    
    public Forma3D(ArrayList<Ponto3D> pontos, Color cor){
        super(cor);
        this.pontos = new Ponto3D[pontos.size()];
        this.pontos = pontos.toArray(this.pontos);
    }
    
    public Forma3D(ArrayList<Ponto3D> pontos, int [][] faces, Color cor){
        super(cor);
        this.pontos = new Ponto3D[pontos.size()];
        this.pontos = pontos.toArray(this.pontos);

        this.faces = faces.clone();
    }
    
    public Forma3D(ArrayList<Ponto3D> pontos, ArrayList<ArrayList<Integer>> faces, Color cor){
        super(cor);
        this.pontos = new Ponto3D[pontos.size()];
        this.pontos = pontos.toArray(this.pontos);
        
        this.faces = new int[faces.size()][];
        int tamanho;
        
        for (int i = 0 ; i < faces.size() ; i++){
            tamanho = faces.get(i).size();
            this.faces[i] = new int[tamanho];
            for (int j = 0 ; j < tamanho ; j++){
                this.faces[i][j] = faces.get(i).get(j);
            }
        }
    }
    
    @Override
    public void desenha() {
        //testar
        for (int i = 0 ; i < faces.length ; i++){
            for (int j = 0 ; j < faces[i].length ; j++ ){
                Funcao.desenhaLinha(pontos[faces[i][j]], pontos[faces[i][(j+1) % faces[i].length]], cor);
            }
        }
    }
    
    @Override
    public void ajustar(double [][] matriz){
        
        for (int i = 1 ; i < pontos.length + 1; i++){
            pontos[i - 1].x = (int) matriz[0][i];
            pontos[i - 1].y = (int) matriz[1][i];
            pontos[i - 1].z = (int) matriz[2][i];
        }
    }
    
    @Override
    public double[][] getMatriz(){
        double [][] matriz = new double[4][pontos.length + 1];
        
        Ponto3D c = centroide(pontos);
        matriz[0][0] = c.x;
        matriz[1][0] = c.y;
        matriz[2][0] = c.z;
        matriz[3][0] = 1;
        
        for (int i = 1 ; i < pontos.length + 1 ; i++){
            matriz[0][i] = pontos[i - 1].x;
            matriz[1][i] = pontos[i - 1].y;
            matriz[2][i] = pontos[i - 1].z;
            matriz[3][i] = 1;
        }
        
        return matriz;
    }
    
    @Override
    public Ponto3D[] getPontos() {
        return pontos;
    }
    
    @Override
    public String toString(){
        return "Forma 3D | " + super.toString() + " | Qtd. pontos=" + pontos.length;
    }
}
