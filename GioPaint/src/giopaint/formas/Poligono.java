package giopaint.formas;

import giopaint.Ponto3D;
import giopaint.funcoes.Funcao;
import java.awt.Color;

public class Poligono extends Forma {
    public Ponto3D meio;
    public int raio;
    public int lados;
    Ponto3D [] pontos;
    
    public Poligono(){
        super();
        meio = new Ponto3D();
        raio = 1;
        lados = 3;
        definePontos();
    }
    
    public Poligono(Ponto3D meio, int lados, int raio){
        super();
        this.meio = new Ponto3D(meio);
        this.lados = lados;
        this.raio = raio;
        definePontos();
    }
    
    public Poligono(int x, int y, int lados, int raio){
        super();
        meio = new Ponto3D(x, y);
        this.lados = lados;
        this.raio = raio;
        definePontos();
        
    }
    
    public Poligono(Ponto3D meio, int lados, int raio, Color cor){
        super(cor);
        this.meio = new Ponto3D(meio);
        this.lados = lados;
        this.raio = raio;
        definePontos();
    }
    
    public Poligono(int x, int y, int lados, int raio, Color cor){
        super(cor);
        meio = new Ponto3D(x, y);
        this.lados = lados;
        this.raio = raio;
        definePontos();
    }
    
    private void definePontos(){
        pontos = new Ponto3D[lados];
        int x, y, i;
        for (i = 0; i < lados; i++) {
            x = (int) (meio.x + raio * Math.cos(i * 2 * Math.PI / lados));
            y = (int) (meio.y + raio * Math.sin(i * 2 * Math.PI / lados));
            pontos[i] = new Ponto3D(x,y);
        }
    }
    
    @Override
    public void desenha() {
        for (int i = 0 ; i < lados ; i++){
            Funcao.desenhaLinha(pontos[i], pontos[(i+1) % lados], cor);
        }
    }
    
    @Override
    public void ajustar(double [][] matriz){
        meio.x = (int) matriz[0][0];
        meio.y = (int) matriz[1][0];
        meio.z = (int) matriz[2][0];
        
        for (int i = 1 ; i < pontos.length + 1; i++){
            pontos[i - 1].x = (int) matriz[0][i];
            pontos[i - 1].y = (int) matriz[1][i];
            pontos[i - 1].z = (int) matriz[2][i];
        }
    }
    
    @Override
    public double[][] getMatriz(){
        double [][] matriz = new double[4][pontos.length + 1];
        
        matriz[0][0] = meio.x;
        matriz[1][0] = meio.y;
        matriz[2][0] = meio.z;
        matriz[3][0] = 1;
        
        for (int i = 1 ; i < pontos.length + 1 ; i++){
            matriz[0][i] = pontos[i - 1].x;
            matriz[1][i] = pontos[i - 1].y;
            matriz[2][i] = pontos[i - 1].z;
            matriz[3][i] = 1;
        }
        
        return matriz;
    }
    
    @Override
    public Ponto3D[] getPontos() {
        return pontos;
    }
    
    @Override
    public String toString(){
        return "Polígono regular | " + super.toString() + " | Qtd. pontos=" + pontos.length;
    }
}
