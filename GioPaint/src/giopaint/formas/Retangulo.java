package giopaint.formas;

import giopaint.Ponto3D;
import giopaint.funcoes.Funcao;
import java.awt.Color;

public class Retangulo extends Forma {
    public Ponto3D pontos[] = new Ponto3D[4];
    
    public Retangulo(){
        super();
        for (int i = 0 ; i < 4 ; i++)
            pontos[i] = new Ponto3D();
    }
    
    public Retangulo(Ponto3D p1, Ponto3D p2){
        super();
        pontos[0] = new Ponto3D(p1);
        pontos[1] = new Ponto3D(p1.x, p2.y);
        pontos[2] = new Ponto3D(p2);
        pontos[3] = new Ponto3D(p2.x, p1.y);
    }
    
    public Retangulo(Ponto3D p1, Ponto3D p2, Color cor){
        super(cor);
        pontos[0] = new Ponto3D(p1);
        pontos[1] = new Ponto3D(p1.x, p2.y);
        pontos[2] = new Ponto3D(p2);
        pontos[3] = new Ponto3D(p2.x, p1.y);
    }
    
    public Retangulo(int x1, int y1, int x2, int y2){
        super();
        pontos[0] = new Ponto3D(x1, y1);
        pontos[1] = new Ponto3D(x1, y2);
        pontos[2] = new Ponto3D(x2, y2);
        pontos[3] = new Ponto3D(x2, y1);
    }
    
    public Retangulo(int x1, int y1, int x2, int y2, Color cor){
        super(cor);
        pontos[0] = new Ponto3D(x1, y1);
        pontos[1] = new Ponto3D(x1, y2);
        pontos[2] = new Ponto3D(x2, y2);
        pontos[3] = new Ponto3D(x2, y1);
    }
    
    @Override
    public void desenha() {
        for (int i = 0 ; i < pontos.length ; i++){
            Funcao.desenhaLinha(pontos[i], pontos[(i+1) % pontos.length], cor);
        }
    }
    
    @Override
    public void ajustar(double [][] matriz){
        for (int i = 0 ; i < pontos.length ; i++){
            pontos[i].x = (int) matriz[0][i];
            pontos[i].y = (int) matriz[1][i];
            pontos[i].z = (int) matriz[2][i];
        }
    }
    
    @Override
    public double[][] getMatriz(){
        double [][] matriz = new double[4][pontos.length];
        
        for (int i = 0 ; i < pontos.length ; i++){
            matriz[0][i] = pontos[i].x;
            matriz[1][i] = pontos[i].y;
            matriz[2][i] = pontos[i].z;
            matriz[3][i] = 1;
        }
        
        return matriz;
    }
    
    @Override
    public Ponto3D[] getPontos() {
        return pontos;
    }
    
    @Override
    public String toString(){
        String s = "Retângulo | " + super.toString();
        for (Ponto3D x: pontos) s += " | " + x;
        return s;
    }
}
