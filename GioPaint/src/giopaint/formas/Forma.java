package giopaint.formas;

import giopaint.Ponto3D;
import java.awt.Color;

public abstract class Forma {
    public Color cor;
    
    public Forma(){
        cor = Color.BLACK;
    }
    
    public Forma(Color cor){
        this.cor = cor;
    }
    
    public static Ponto3D centroide(Ponto3D [] pontos){
        int x = 0;
        int y = 0;
        int z = 0;
        
        for (Ponto3D w : pontos){
            x += w.x;
            y += w.y;
            z += w.z;
        }

        x /= pontos.length;
        y /= pontos.length;
        z /= pontos.length;

        return new Ponto3D(x, y, z);
    }
    
    public abstract void desenha();
    public abstract Ponto3D[] getPontos();
    public abstract void ajustar(double [][] matriz);
    public abstract double[][] getMatriz();
    
    @Override
    public String toString(){
        return "Cor:(" + cor.getRed() + "," + cor.getGreen() + "," + cor.getBlue() + ")";

    }
}