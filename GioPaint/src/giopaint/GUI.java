package giopaint;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

class GUI extends JFrame {
    
    private javax.swing.JMenuBar barraMenu;
    private javax.swing.JMenu menuAdicionar;
    private javax.swing.JMenu menuArquivo;
    private javax.swing.JMenu menuEditar;
    private javax.swing.JMenu menuProjetar;
    private javax.swing.JMenu menuAjuda;
    private javax.swing.JMenuItem menuCirculo;
    private javax.swing.JMenuItem menuElipse;
    private javax.swing.JMenuItem menuEscalar;
    private javax.swing.JMenuItem menuExcluir;
    private javax.swing.JMenuItem menuForma3D;
    private javax.swing.JMenuItem menuLinha;
    private javax.swing.JMenuItem menuNovo;
    private javax.swing.JMenuItem menuPoligono;
    private javax.swing.JMenuItem menuPolilinha;
    private javax.swing.JMenuItem menuPolilinhaAberta;
    private javax.swing.JMenuItem menuPreencher;
    private javax.swing.JMenuItem menuProjecao;
    private javax.swing.JMenuItem menuRecortar;
    private javax.swing.JMenuItem menuRetangulo;
    private javax.swing.JMenuItem menuRotacionar;
    private javax.swing.JMenuItem menuSalvar;
    private javax.swing.JMenuItem menuTransladar;
    private javax.swing.JMenuItem menuProjecaoObliqua;
    private javax.swing.JMenuItem menuProjecaoIsometrica;
    private javax.swing.JMenuItem menuProjecao2Pontos;
    private javax.swing.JMenuItem menuProjecao3Pontos;
    private javax.swing.JMenuItem menuManual;
    private javax.swing.JMenuItem menuSobre;
    
    

    public GUI() throws UnsupportedLookAndFeelException{
        add(Display.instancia());
        setContentPane(Display.instancia());
        pack();
        
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);

        iniciarComponentes();
    }
    
    
    private void iniciarComponentes(){
        
        setTitle("GioPaint 1.0");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        barraMenu = new javax.swing.JMenuBar();
        menuArquivo = new javax.swing.JMenu();
        menuNovo = new javax.swing.JMenuItem();
        menuSalvar = new javax.swing.JMenuItem();
        menuAdicionar = new javax.swing.JMenu();
        menuLinha = new javax.swing.JMenuItem();
        menuRetangulo = new javax.swing.JMenuItem();
        menuPolilinha = new javax.swing.JMenuItem();
        menuPolilinhaAberta = new javax.swing.JMenuItem();
        menuPoligono = new javax.swing.JMenuItem();
        menuCirculo = new javax.swing.JMenuItem();
        menuElipse = new javax.swing.JMenuItem();
        menuForma3D = new javax.swing.JMenuItem();
        menuEditar = new javax.swing.JMenu();
        menuExcluir = new javax.swing.JMenuItem();
        menuPreencher = new javax.swing.JMenuItem();
        menuRecortar = new javax.swing.JMenuItem();
        menuRotacionar = new javax.swing.JMenuItem();
        menuEscalar = new javax.swing.JMenuItem();
        menuTransladar = new javax.swing.JMenuItem();
        menuProjetar = new javax.swing.JMenu();
        menuProjecao = new javax.swing.JMenuItem();
        menuProjecaoObliqua = new javax.swing.JMenuItem();
        menuProjecaoIsometrica = new javax.swing.JMenuItem();
        menuProjecao2Pontos = new javax.swing.JMenuItem();
        menuProjecao3Pontos = new javax.swing.JMenuItem();
        menuAjuda = new javax.swing.JMenu();
        menuManual = new javax.swing.JMenuItem();
        menuSobre = new javax.swing.JMenuItem();
        
        
        menuArquivo.setText("Arquivo");

        menuNovo.setText("Novo");
        menuNovo.addActionListener(this::menuNovoActionPerformed);
        menuArquivo.add(menuNovo);

        menuSalvar.setText("Salvar");
        menuSalvar.addActionListener(this::menuSalvarActionPerformed);
        menuArquivo.add(menuSalvar);

        barraMenu.add(menuArquivo);

        menuAdicionar.setText("Adicionar");

        menuLinha.setText("Linha");
        menuLinha.addActionListener(this::menuLinhaActionPerformed);
        menuAdicionar.add(menuLinha);

        menuRetangulo.setText("Retângulo");
        menuRetangulo.addActionListener(this::menuRetanguloActionPerformed);
        menuAdicionar.add(menuRetangulo);
        
        menuPolilinhaAberta.setText("Polígono aberto");
        menuPolilinhaAberta.addActionListener(this::menuPolilinhaAbertaActionPerformed);
        menuAdicionar.add(menuPolilinhaAberta);

        menuPolilinha.setText("Polígono");
        menuPolilinha.addActionListener(this::menuPolilinhaActionPerformed);
        menuAdicionar.add(menuPolilinha);

        menuPoligono.setText("Polígono Regular");
        menuPoligono.addActionListener(this::menuPoligonoActionPerformed);
        menuAdicionar.add(menuPoligono);

        menuCirculo.setText("Círculo");
        menuCirculo.addActionListener(this::menuCirculoActionPerformed);
        menuAdicionar.add(menuCirculo);
        
        menuElipse.setText("Elipse");
        menuElipse.addActionListener(this::menuElipseActionPerformed);
        menuAdicionar.add(menuElipse);

        menuForma3D.setText("Forma 3D");
        menuForma3D.addActionListener(this::menuForma3DActionPerformed);
        menuAdicionar.add(menuForma3D);

        barraMenu.add(menuAdicionar);

        menuEditar.setText("Editar");

        menuPreencher.setText("Preencher");
        menuPreencher.addActionListener(this::menuPreencherActionPerformed);
        menuEditar.add(menuPreencher);

        menuRecortar.setText("Recortar");
        menuRecortar.addActionListener(this::menuRecortarActionPerformed);
        menuEditar.add(menuRecortar);
        
        menuExcluir.setText("Excluir...");
        menuExcluir.addActionListener(this::menuExcluirActionPerformed);
        menuEditar.add(menuExcluir);

        menuRotacionar.setText("Rotacionar...");
        menuRotacionar.addActionListener(this::menuRotacionarActionPerformed);
        menuEditar.add(menuRotacionar);

        menuEscalar.setText("Escalar...");
        menuEscalar.addActionListener(this::menuEscalarActionPerformed);
        menuEditar.add(menuEscalar);

        menuTransladar.setText("Transladar...");
        menuTransladar.addActionListener(this::menuTransladarActionPerformed);
        menuEditar.add(menuTransladar);
        
        barraMenu.add(menuEditar);
        
        menuProjetar.setText("Projetar");

        menuProjecao.setText("Projeção...");
        menuProjecao.addActionListener(this::menuProjecaoActionPerformed);
        menuProjetar.add(menuProjecao);

        menuProjecaoObliqua.setText("Projeção Oblíqua...");
        menuProjecaoObliqua.addActionListener(this::menuProjecaoObliquaActionPerformed);
        menuProjetar.add(menuProjecaoObliqua);
        
        menuProjecaoIsometrica.setText("Projeção Isométrica...");
        menuProjecaoIsometrica.addActionListener(this::menuProjecaoIsometricaActionPerformed);
        menuProjetar.add(menuProjecaoIsometrica);
        
        menuProjecao2Pontos.setText("Projeção com 2 pontos de fuga...");
        menuProjecao2Pontos.addActionListener(this::menuProjecao2PontosActionPerformed);
        menuProjetar.add(menuProjecao2Pontos);

        menuProjecao3Pontos.setText("Projeção com 3 pontos de fuga...");
        menuProjecao3Pontos.addActionListener(this::menuProjecao3PontosActionPerformed);
        menuProjetar.add(menuProjecao3Pontos);

        barraMenu.add(menuProjetar);
        
        menuAjuda.setText("Ajuda");
        
        menuManual.setText("Manual");
        menuManual.addActionListener(this::menuManualActionPerformed);
        menuAjuda.add(menuManual);
        
        menuSobre.setText("Sobre");
        menuSobre.addActionListener(this::menuSobreActionPerformed);
        menuAjuda.add(menuSobre);
        
        barraMenu.add(menuAjuda);

        setJMenuBar(barraMenu);
        
        pack();
    }
    
    private void menuNovoActionPerformed(java.awt.event.ActionEvent evt) {                                         
        Display.instancia().apagarTudo();
    }                                        

    private void menuSalvarActionPerformed(java.awt.event.ActionEvent evt) {                                           
        Display.instancia().salvar();
    }                                          

    private void menuLinhaActionPerformed(java.awt.event.ActionEvent evt) {  
        Display.instancia().mudarModo(Display.Modo.Linha);
    }                                         

    private void menuRetanguloActionPerformed(java.awt.event.ActionEvent evt) {                                              
        Display.instancia().mudarModo(Display.Modo.Retangulo);
    }
    
    private void menuPolilinhaAbertaActionPerformed(java.awt.event.ActionEvent evt) {                                              
        Display.instancia().mudarModo(Display.Modo.PolilinhaAberta);
    }  

    private void menuPolilinhaActionPerformed(java.awt.event.ActionEvent evt) {                                              
        Display.instancia().mudarModo(Display.Modo.Polilinha);
    }                                             

    private void menuPoligonoActionPerformed(java.awt.event.ActionEvent evt) {                                             
        Display.instancia().mudarModo(Display.Modo.Poligono);
    }                                            

    private void menuElipseActionPerformed(java.awt.event.ActionEvent evt) {                                           
        Display.instancia().mudarModo(Display.Modo.Elipse);
    }
    
    private void menuCirculoActionPerformed(java.awt.event.ActionEvent evt) {                                           
        Display.instancia().mudarModo(Display.Modo.Circulo);
    }

    private void menuForma3DActionPerformed(java.awt.event.ActionEvent evt) {                                            
        Display.instancia().mudarModo(Display.Modo.Forma3D);
    }                                           

    private void menuExcluirActionPerformed(java.awt.event.ActionEvent evt) {                                            
        Display.instancia().mudarModo(Display.Modo.Remover);
    }                                           

    private void menuPreencherActionPerformed(java.awt.event.ActionEvent evt) {                                              
        Display.instancia().mudarModo(Display.Modo.Preencher);
    }                                             

    private void menuRecortarActionPerformed(java.awt.event.ActionEvent evt) {                                             
        Display.instancia().mudarModo(Display.Modo.Recorte);
    }                                            

    private void menuRotacionarActionPerformed(java.awt.event.ActionEvent evt) {                                               
        Display.instancia().mudarModo(Display.Modo.Rotacionar);
    }                                              

    private void menuEscalarActionPerformed(java.awt.event.ActionEvent evt) {                                            
        Display.instancia().mudarModo(Display.Modo.Escalar);
    }                                           

    private void menuTransladarActionPerformed(java.awt.event.ActionEvent evt) {                                               
        Display.instancia().mudarModo(Display.Modo.Transladar);
    }                                              

    private void menuProjecaoActionPerformed(java.awt.event.ActionEvent evt) {                                             
        Display.instancia().mudarModo(Display.Modo.Projecao);
    }
    
    private void menuProjecaoObliquaActionPerformed(java.awt.event.ActionEvent evt) {                                             
        Display.instancia().mudarModo(Display.Modo.ProjecaoObliqua);
    }
    
    private void menuProjecaoIsometricaActionPerformed(java.awt.event.ActionEvent evt) {                                             
        Display.instancia().mudarModo(Display.Modo.ProjecaoIsometrica);
    }
    
    private void menuProjecao2PontosActionPerformed(java.awt.event.ActionEvent evt) {                                             
        Display.instancia().mudarModo(Display.Modo.Projecao2Pontos);
    }
    
    private void menuProjecao3PontosActionPerformed(java.awt.event.ActionEvent evt) {                                             
        Display.instancia().mudarModo(Display.Modo.Projecao3Pontos);
    }
    
    private void menuManualActionPerformed(java.awt.event.ActionEvent evt) {                                             
        Display.instancia().manual();
    }
    
    private void menuSobreActionPerformed(java.awt.event.ActionEvent evt) {                                             
        Display.instancia().sobre();
    }
    
    public static void main(String[] args) {
        
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e){}
        
        java.awt.EventQueue.invokeLater(() -> {
            try {
                new GUI().setVisible(true);
            } catch (UnsupportedLookAndFeelException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

}