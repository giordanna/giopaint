package giopaint;

import java.awt.Color;
import java.awt.Point;

public class Pixel extends Point {
    public Color cor;
    
    public Pixel(Point ponto, Color cor){
        super();
        this.x = ponto.x;
        this.y = ponto.y;
        this.cor = new Color(cor.getRed(), cor.getGreen(), cor.getBlue(), cor.getAlpha());
    }
    
    public Pixel(int x, int y, Color cor){
        super();
        this.x = x;
        this.y = y;
        this.cor = new Color(cor.getRed(), cor.getGreen(), cor.getBlue(), cor.getAlpha());
    }
    
    public Pixel(Pixel copia){
        super();
        this.x = copia.x;
        this.y = copia.y;
        this.cor = copia.cor;
    }
    
    public Pixel(Point ponto, int red, int green, int blue, int alpha){
        super();
        this.x = ponto.x;
        this.y = ponto.y;
        this.cor = new Color(red, green, blue, alpha);
    }
    
    public Pixel(int x, int y, int red, int green, int blue, int alpha){
        super();
        this.x = x;
        this.y = y;
        this.cor = new Color(red, green, blue, alpha);
    }
}
